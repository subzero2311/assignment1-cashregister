## 1.  Introduction   
This is the manual for the Cash Register application. Here you will find instructions on how to download, install and use the software.
## 2.  How to download the application   
   Paste the following url in your favorite web browser: https://gitlab.com/subzero2311/assignment1-cashregister.git.   
   Click on the download button and then select you prefered compression format. If you are unsure I recomend the zip format.   
## 3.  How to install the application
   The application file is in a comressed format and needs to be extracted into a folder. Depending on which file type you have downloaded will depend on what compression software you can use. I will be explaining zip format here. If you want to find out how to extract other file types google.com is your friend.
### Extracting a zip file in Windows
Loacate folder where you downloaded assignment1-cashregister-master.zip file. 
To unzip you need to press and hold (or right-click) the folder, select Extract All, and then follow the instructions of the Wizard that is displayed.
Open a command prompt window and go to the directory where you extracted the java program
then enter the following commands

> cd source   
> javac CashRegister.java   

### Extracting a zip file in Linux
First you need to install unzip as it not usually installed on most ditributions of linux. You need to install unzip from the command line.   
To install unzip on Ubunto and Debian you can enter:   

> $ sudo apt install unzip   

To Install unzip on CentOS and Fedora you can enter   

> $ sudo yum install unzip   

To extract a ZIP archive into a specific directory, the user needs to have write permissions on that directory.   

Got to you current directory where you downloaded the file and then enter:   

> unzip assignment1-cashregister-master.zip   

This will unzip to your current directory
You then need to compile the program by entering

> $ cd source   
> $ make   

## 4.  How to use the application
1.  When application runs,user is given a choice to process a transaction or exit.
      * If user wishes to exit, the cash register will show the balance of cash register and exit the application.
      * If user wishes to continue, user will press p to continue to process a transaction.
2.  User is prompted to enter the items name, cost and the amount of that item.
3.  User is prompted to enter any more different items that they wish.
      * If user wishes to add items, they will be prompted back to the transaction screen.
4.  User is prompted to add Discount coupons percentage if they own any.
5.  User can choose from different range of cash choices to pay for the items, from 50 dollars to 0.10 cents
      * User picks corresponding cash amount to the item price.
6.  User is shown the receipt for the current transaction, including the items bought, the price, the price after discount and the change required.
7.  Intrusctions go back to No.1 if user wants to process the transaction.
