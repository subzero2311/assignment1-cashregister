import java.io.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author erico
 */
public class FileManager<T> {
	
	public void writeFile(String path, T object) {
		
        try {
            FileOutputStream fos = new FileOutputStream(path);
         ObjectOutputStream oos = new ObjectOutputStream(fos);
         oos.writeObject(object);
		  
         oos.close();
         fos.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    
   @SuppressWarnings("unchecked")
	public T readFile(String path, T object) {
        
   	try
      {
          FileInputStream fis = new FileInputStream(path);
          ObjectInputStream ois = new ObjectInputStream(fis);

          object = (T) ois.readObject();

          ois.close();
          fis.close();
      }
      catch (FileNotFoundException ee){
   	System.out.println("File not Found");
      }
      catch (IOException ioe)
      {
   	System.out.println("Error initializing stream");
          ioe.printStackTrace();
      }
      catch (ClassNotFoundException c)
      {
          System.out.println("Class not found");
          c.printStackTrace();
      }
   	return object;
    }
   

}
