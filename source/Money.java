import java.io.Serializable;

/**
 * 
 * @author Sandy Nelson
 * Money class: Used to manage a money types. 
 */
public class Money implements Serializable{
	
	private static final long serialVersionUID = 1L;
	// constants for the different types of money
	public static final double FIFTY_DOLLARS = 50;
	public static final double TWENTY_DOLLARS = 20;
	public static final double TEN_DOLLARS = 10;
	public static final double FIVE_DOLLARS = 5;
	public static final double TWO_DOLLARS = 2;
	public static final double ONE_DOLLAR = 1;
	public static final double FIFTY_CENTS = 0.5;
	public static final double TWENTY_CENTS = 0.2;
	public static final double TEN_CENTS = 0.1;
	public static final double FIVE_CENTS = 0.05;
	
	// type of currency
	private double _type;
	// amount of the type of currency
	private int _amount;
	
	/**
	 * @param type the _type to set
	 * @param amount the _amount to set
	 */
	public Money(double type, int amount) {
		super();
		_type = type;
		_amount = amount;
	}

	/**
	 * @return the _type
	 */
	public double getType() {
		return _type;
	}

	/**
	 * @param type the _type to set
	 */
	public void setType(double type) {
		_type = type;
	}

	/**
	 * @return the _amount
	 */
	public int getAmount() {
		return _amount;
	}

	/**
	 * @param amount the _amount to set
	 */
	public void setAmount(int amount) {
		_amount = amount;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Money [_type=");
		builder.append(_type);
		builder.append(", _amount=");
		builder.append(_amount);
		builder.append("]");
		return builder.toString();
	}
}
