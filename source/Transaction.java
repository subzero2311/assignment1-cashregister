public class Transaction
{
   private String name;
   private double cost;
   private int amount;

   public Transaction(String name, double cost, int amount) {
      this.name = name;
      this.cost = cost;
      this.amount = amount;
   }

   public double getCost() {
      return cost;
   }

   public void setCost(double cost) {
      this.cost = cost;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }
   
   public int getAmount() {
      return amount;
   }
	
   public void setAmount(int amount) {
      this.amount = amount;
   }

		


   //toString to show itemName and itemCost
   @Override
   public String toString()
   {
     return "\n" + name + "\t$" + String.format("%.2f", cost) + "\t" + amount;  
   }
}
