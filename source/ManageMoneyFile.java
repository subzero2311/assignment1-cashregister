import java.util.ArrayList;

/**
 *
 * @author erico
 */
public class ManageMoneyFile {
	String path;
	ArrayList<Money> list;
	
	FileManager<ArrayList<Money>> filemanage = new FileManager<ArrayList<Money>>();
	boolean isEmpty;
	
	public ManageMoneyFile(String path, ArrayList<Money> list){
		this.path = path;
		this.list = list;
	}
	
	//method of readFile
	public ArrayList<Money> readFile(){
		list = filemanage.readFile(path, list);
		return list;
	}
	
	//method of writeFile
	public void writeFile(){
		filemanage.writeFile(path, list);
	}
}