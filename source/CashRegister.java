import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CashRegister
{
   public static void main(String[] args)
   {
   	final String MONEY_FILE = "money.dat";
      String s, c;
      double balance = 0;
		double discountAmount;
		double newTotalCost = 0;
		double discount = 0;
		double amountTendered = 0;
		double changeRequired = 0;
		int itemAmount;
      List<Transaction> transactions = new ArrayList<Transaction>();
      ArrayList<Money> floatMoney = new ArrayList<Money>(); // the dollars and cents denominations
      ManageMoneyFile manageMoneyFile = new ManageMoneyFile(MONEY_FILE, floatMoney);
      ArrayList<Money> tenderedMoney = new ArrayList<Money>();
      Scanner in = new Scanner(System.in);

      System.out.println("Welcome, how can I help you today?");
      
      // determine the balance reading the MONEY_FILE and calculating it
      floatMoney = manageMoneyFile.readFile();
      for (Money money : floatMoney) {
      	balance = balance + money.getType() * money.getAmount();
      }
      
      System.out.println("The current float is: " + String.format("$%.2f", balance));
      
      char menuOption = 'p'; // default Cash Register Menu option
      do {
      	s = readString("Cash Register Menu"
	      		+ "\n (p)rocess a transaction"
	      		+ "\n e(x)it"
	      		+ "\nWhat would you like to do?", in);
      	
   		menuOption = s.toLowerCase().charAt(0);
	      switch(menuOption) {
		      case 'p': // process transaction
		      	transactions.clear(); // new transactions, so clear transactions
		      	char anotherTrans = 'y'; // default anotherTrans to y for first transaction
		      	do {
		      		switch(anotherTrans) {
		      		case 'y': // enter another item
							s = readString("Please enter the item's name:", in);

							itemAmount = (int) readPositiveDouble("How many items would you like:" , in);
							
							double itemCost = readPositiveDouble("Please enter the item's cost:", in);
							
							Transaction trans = new Transaction(s, itemCost, itemAmount);
							transactions.add(trans);
							
							s = readString("Would you like to add another item? (y)es, (n)o:", in);
							anotherTrans = s.toLowerCase().charAt(0);
							break;
		      		case 'n': // stop entering items
		      			break;
		      		default: // wrong character entered
		      			System.out.println("You have entered an invalid character");
							s = readString("Would you like to add another item? (y)es, (n)o:", in);
							anotherTrans = s.toLowerCase().charAt(0);
		      			break;
		      		}
			  	  		
		      	} while (anotherTrans != 'n');
					
					double totalTransCost = 0;
					double newCost = 0;
					
					// calculate the total cost of all the transactions
					for (Transaction transaction : transactions) {
					   newCost = transaction.getAmount() * transaction.getCost();
					   totalTransCost += newCost;
					}
					
					//Discount Amount
					discount = readPositiveDouble("Please enter the discount percentage amount", in);
					
					//calculate discount amount
					discountAmount = 100-discount;
					
					//calculate new total after discount
					newTotalCost = (discountAmount*totalTransCost)/100;
					
					tenderedMoney = initMoney(tenderedMoney);
					// keep asking for the amount tendered until it is >= total transaction cost
					do {
						System.out.println("Amount due: " + String.format("$%.2f" , (newTotalCost - amountTendered)));
						displayTenderedMoney(tenderedMoney);
						String typeTenderedString = readString("Please enter corresponding number of note or coin " +
								"to be tendered:", in);
						int typeTendered = Integer.parseInt(typeTenderedString);
						
						// increment the amount for the corresponding type in tenderedMoney
						if (typeTendered >= 0 && typeTendered <= 9) {
							tenderedMoney.get(typeTendered).setAmount(tenderedMoney.get(typeTendered).getAmount() + 1);
						}
						
						// reset amountTendered and calculate the amountTendered from tenderedMoney
						amountTendered = 0;
						for (Money money : tenderedMoney) {
							amountTendered = amountTendered + money.getType() * money.getAmount();
						}
					} while (amountTendered < newTotalCost);
					
					//calculates change required
					changeRequired = amountTendered - newTotalCost;
					//prints out receipt labels
					System.out.println("\nReceipt" + "\n***********");
					System.out.println("Name" + "\tCost" + "\tAmount");
                                        
					// display each item and cost
					for (Transaction transaction : transactions) {
						System.out.println(transaction.toString());
					}
					
					//prints out the rest of the receipt
					System.out.println("\nTotal Cost: " + String.format("$%.2f", totalTransCost) + "\n"+"Discount Amount applied:" + String.format("%.2f", (100 - discountAmount)) 
							  + "%" +"\n"+"Total Cost After Discount: "+String.format("$%.2f ",newTotalCost) 
							+ "\n" + "Amount Tendered: " + 
							String.format("$%.2f", amountTendered) + "\n" + "Change Required: " + 
							String.format("$%.2f", changeRequired));
					
					//Calculate the new balance of cash register after each transaction
					balance += newTotalCost;
					amountTendered = 0;

					break;
		      case 'x': // exit program
	      		double totalCost = 0 ;

				c = Double.toString(balance);
				
				System.out.println("Balance of the Cash Register: $" + String.format("%.2f", Double.parseDouble(c)));
		      	break;
		      default: // wrong char entered
		      	System.out.println("You have entered an invalid option");
		      	break;
	      } 
      } while (menuOption != 'x');
   }
   
   public static double readPositiveDouble(String message, Scanner inputDouble) {
   	String str;
   	double number = 0;
   	do {
   		System.out.printf("\n%s ", message); // display the message
   		
   		// When Scanner inputDouble is not a double request the value to entered again
   		while (!inputDouble.hasNextDouble()) {
   			System.out.println("The value entered must be a positive floating point value. "
   					+ "Try again!");
   			System.out.printf("\n%s ", message); // re-display the message
   			inputDouble.nextLine(); // returns the previous entered token
   		}
   		
   		str = inputDouble.nextLine();
   		// set the double
   		number = Double.parseDouble(str);
   		
   		// When the number is a negative number inform the user by error message
   		if (number < 0) {
   			System.out.println("The value must not be a negative number. Try Again!");
   		}
   		
   	} while (number < 0); // must not have a negative number
   	
   	//inputDouble.close();
   	return number;
   }
   
   public static String readString(String message, Scanner inputString) {
   	//Scanner inputString = new Scanner(System.in);
   	String str;
   	
   	do {
   		System.out.printf("\n%s ", message); // display the message
   		str = inputString.nextLine();
   		
   		// When str is empty (user just pressed enter) inform the user by error message
   		if (str == null || str.length() == 0) {
   			System.out.println("You did not enter anything, Try again!");
   		}
   		
   	} while (str == "" || str.length() == 0);
   	
   	//inputString.close();
   	return str;
   }
   
   public static void displayTenderedMoney(ArrayList<Money> money) {
   	System.out.println("Amount of money tendered");
   	System.out.println("========================");
   	for (int index = 0; index < money.size(); index++) {
   		System.out.println(" " + index + ") " + money.get(index).getType() + 
   				", " + money.get(index).getAmount()); 
   	}
   }
   
   public static ArrayList<Money> initMoney(ArrayList<Money> money) {
   	money = new ArrayList<Money>();
      money.add(new Money(Money.FIFTY_DOLLARS, 0));
      money.add(new Money(Money.TWENTY_DOLLARS, 0));
      money.add(new Money(Money.TEN_DOLLARS, 0));
      money.add(new Money(Money.FIVE_DOLLARS, 0));
      money.add(new Money(Money.TWO_DOLLARS, 0));
      money.add(new Money(Money.ONE_DOLLAR, 0));
      money.add(new Money(Money.FIFTY_CENTS, 0));
      money.add(new Money(Money.TWENTY_CENTS, 0));
      money.add(new Money(Money.TEN_CENTS, 0));
      money.add(new Money(Money.FIVE_CENTS, 0));
      return money;
   }
} 
