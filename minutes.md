# 1st Meeting Minutes for 12 September 2019
## Meeting started: 6:00pm
## Attendance: Osama Momtaz Baaj, Eric Foo, Sandy Nelson
Sandy Nelson created the project Assignment1-CashRegister and assigned all the members to have Maintainer privileges.
Created Milestone 1 – Fixing the bugs
Created Milestone 2 – Adding Features
 We discussed Milestone 1 issues and decided who will be responsible them.
###Opened the following issues for Milestone 1:
* #1No dollar signs before cash values - Osama Momtaz Baaj
* #2Ending balance of cash register is not displaying – Sandy Nelson
* #3Change value is not correct – Eric Foo
* #4Spelling errors in prompts – Eric Foo
* #5Can't compile using the Makefile – Sandy Nelson

## Meeting ended 7:15 pm

# 2nd Meeting Minutes for 14 September 2019
## Meeting Started: 6:00 pm
## Attendance: Eric Foo, Sandy Nelson
Osama Momtaz Baaj has left the group for personal reasons  
We discussed who will be doing Osama Momtaz Baaj issue.  
We agreed that Sand Nelson will do issue: No dollar signs before cash values  
We discussed Milestone 2 issues and decided who will be responsible them.  
### Opened the following issues for Milestone 2:
* #6 Modify the Makefile to include creation of a jar file – Sandy Nelson
* #7 Create a branch for an experimental version of the software in another language – Eric Foo
* #8 Add a nice welcome message once the program is started – Eric Foo
* #9 Add eloquent error handling in the case of unexpected input from the user – Sandy Nelson
* #10 Allow multiple items to be bought at the same time – Sandy Nelson
* #11 Remove the display of the cash register balance after each transaction and only display at the end when the user quits – Eric Foo
* #12 Allow for processing of another transaction – Sandy Nelson
* #13 Shows receipt at the end of transactions – Eric Foo

We decided create our development branches in GitLab  
based on our Issues. So that GitLab gives that branch the same reference number as the issue.
We also decided that all our commits inside a given branch would have the refence number included so that it can be referenced back to the branch and the issue.
These decisions were decided based on discussions with Ivan Lee before this meeting.
## Meeting Ended: 8:05 pm ##

# 3rd Meeting Minutes for 2 October 2019
## Meeting Started 6:00 pm
Discussed for many hours trying to work what additional features could implement for Milestone 3
We came up with 10 feature that we could implement and created issues for each of them delegated them to team members
* #14 Bug: Balance not updated after each transaction – Eric Foo
* #15 Bug: Tendered amount can be less that the total transaction amount – Sandy Nelson
* #16 Display all currency values rounded to 2 decimal places – Sandy Nelson
* #17 Allow for entry of a coupon percentage discount before asking for the cash amount tendered – Eric Foo
* #18 Create a Money class – Sandy Nelson
* #19 Create a generic Serializer class – Eric Foo
* #20 Write a class ManageMoneyFile to read from and write to a file called money.dat
* #21 Read money.dat with MangeMoneyFile object to calculate the float amount
* #22 Ask for the amount tendered in denominations of coins and notes
* #23 Allow for entry of multiple items

Note: Details of the reasons to why we came up with these issues can be found in the changes.md file.
## Meeting Ended 8:03pm


# 4th Meeting Minutes for 3 October 2019 #
## Meeting started 6:00 pm
   Discussed how we are to implement and complete our 3 wikis manual.md, changes.md minutes.md in GitLab.   
   We decided to put manual.md, changes.md and minutes.md in the root director of our project. This task was delegated to Sand Nelson   
   We decided that this was the best way because we can edit these files directly in GitLab. Also GitLab provides an edit and preview so we can seewhat the changes look like before we commit.   
   We agreed that the tasks for the wiki will be delegated like this:   
feature.md – features or issues in Milestone 3 that that person is responsible for. See last meeting minutes on 2 October 2018   
### manual.md
* download and install – Sandy Nelson
* how to user the software – Eric Foo

### minutes.md
* first meeting – Eric Foo
* second meeting – Sandy Nelson
* third meeting -Eric Foo
* fourth meeting – Sandy Nelson
## Meeting finished 8.17 pm
