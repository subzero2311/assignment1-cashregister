Group members Eric Foo and Sandy Nelson has chosen some new features to add functionality the Cash Register program. We have discussed amongst each other to implement 10 features/bugs. Here are the following features/bug we have implemented. Here are the Issues that opened for completing the Features/Bugs which form Milestone 3.  

# Sandy Nelson’s Features
## Issue: #15 Bug: Tendered amount can be less that the total transaction amount
The user should not be allowed to tender amounts less than the total transaction amount. This would not be possible in real life.
## Issue: #16 Display all currency values rounded to 2 decimal places
Currency is represented with 2 decimal place so it made sense to add this features to all currency amounts
## Issue: #18 Create a Money class
   The Money class will have accessor and mutator methods with the following variables public static final double HUNDRED_DOLLARS = 100   
> public static final double FIFTY_DOLLARS = 50   
> public static final double TWENTY_DOLLARS = 20   
> public static final double TEN_DOLLARS = 10   
> public static final double FIVE_DOLLARS = 5   
> public static final double TWO_DOLLARS = 2   
> public static final double ONE_DOLLAR = 1   
> public static final double FIFTY_CENTS = 0.5   
> public static final double TWENTY_CENTS = 0.2   
> public static final double TEN_CENTS = 0.1   
> public static final double FIVE_CENTS = 0.05   
> private double type // type of currency ie. FIVE_DOLLARS   
> private int amount // the amount of type   

We as a group that it would be nice to have the ability to enter the tendered amount as denominations so we though that it would be appropriate to be able to keep track of a single denominaton. This later was used for our arrays to keep track of every denomination.
## Issue: #21 Read money.dat with MangeMoneyFile object to calculate the float amount
> In CashRegister class instead of asking for the float amount, calculate the float from the money.dat. Remove the following line:   
> balance = readPositiveDouble("Please enter cash register's float:", in);   
> add the calculated float amount to balance.   
> Use an ArrayList floatMoney to read the money.dat   

We decided it would be nice to have an ability read the current float from a file instead of allowing the user to enter the float each time. 
## Issue: #22 Ask for the amount tendered in denominations of coins and notes.
> When asking for the amount tendered display a menu showing all the types dollars and cents with the amount.   
> Initailly all amounts = 0   
> Keep asking the user to enter more types each time recalculating the amountTendered. unitl the amountTendered greater than or equal to totalTransCost. Each time a type is entered, redisplay the updated main menu. Use an "ArrayList tenderedMoney" to keep track of the types and amounts.   

We wanted to way keep track of the coins and the notes that the user enterd in one by one until the reached the the total transation cost. 
# Eric Foo’s Features
## Issue: #14 Bug: Balance not updated after each transaction
> update the cash register balance after every transaction.

## Issue: #17 Allow for entry of a coupon percentage discount before asking for the cash amount tendered
Ask for the coupon percentage discount to be entered before asking the the amount tendered. Calculate the pencentage off the total transaction amount and then round the transaction total to the nearest 5 cents. 2 cents or less round down. 3 cents or more round up.

We as a group decided to implement a discount option to give the user to make items cheaper.

## Issue: #18 Create a generic Serializer class
This will have a method to serialize any object to a file and method to deserialize a file to an object. Must use generic types. ie.
* public class FileManager {
* public void ReadFile(T object) { }
* public void WriteFile(T object) { }
* }

We decided as a group to make a file so save cash amounts to make it easier for the user to choose the payment options.

## Issue: #20 Write a class ManageMoneyFile to read from and write to a file called money.dat
Create a class file called ManageMoneyFile. Must use the methods provided in the serializer deserializer class created in Issue 6.
This will have two methods:
* public Read()
* public Write()
constructor:
* public ManagerMondeyFile(String path, ArrayList)

Like the issue from aboe no.17 this is the file that will read and write the file to let the user to choose the amount.
Make sure you create the money.dat with the following initial data (Required for issue 8)
* 50, 5
* 20, 5
* 10, 5
* 5, 5
* 2, 5
* 1, 10
* 0.5, 10
* 0.2, 10
* 0.1, 10
* 0.05, 10

As a group we decided to give thees amounts to better a user experience for the user.

## Issue: #23 Allow for entry of multiple items
Modify the Transaction class to have an additional variable:
private int amount // for storing the amount of a particular item
* modify constructor to have amount as a parameter and add assessor and mutator methods for amount
* update the toString method as it is used for printing the receipt
* After the price for the item has been entered. Ask "how many of these items would you like?"
* make sure calculations are correct.

We decided as a group, since it was possible for the user to add mutliple items, we decided to add a function to allow to add mutliple instances of the same item so that u dont need keep adding it and that will waste space.




